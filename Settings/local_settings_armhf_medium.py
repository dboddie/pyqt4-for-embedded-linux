env = {
    "HOST_PLATFORM": "amd64",
    "TARGET_TOOLS": "/usr",
    "NATIVE_TOOLS": "/tmp/i386-toolchain",
    "TARGET_ARCH": "armhf",
    "TARGET_PLATFORM": "arm-linux-gnueabihf",
    "TARGET_PLATFORM_CC": "arm-linux-gnueabihf-gcc",
    "TARGET_PLATFORM_CXX": "arm-linux-gnueabihf-g++",
    "TARGET_PLATFORM_LDSHARED": "arm-linux-gnueabihf-g++",
    "TARGET_PLATFORM_SPEC": "arm-linux-gnueabihf-g++",
    "TARGETDIR": "/usr/local/PyQt-Embedded",
    "HOST_COMPILERS": "1",
    "TARGET_COMPILERS": "1"
}

# Medium ARM build:
qt_options = ["-opensource",
"-release",
"-xplatform qws/" + env["TARGET_PLATFORM_SPEC"],
"-embedded arm",
"-little-endian",
"-no-qvfb",
"-depths 16,32",
"-qt-libjpeg",
"-qt-libmng",
"-qt-libpng",
"-qt-zlib",
"-qt-mouse-linuxtp",
"-no-accessibility",
"-no-qt3support",
"-no-script",
"-no-scripttools",
"-no-webkit",
"-no-declarative"]

qt_configuration_profile = "medium"

extra_qt_configuration = [
"QT_NO_ACCESSIBILITY", "QT_NO_CURSOR", "QT_NO_QWS_CURSOR",
"QT_NO_SESSIONMANAGER", "QT_QWS_SCREEN_COORDINATES"
]

sip_target_platform = env["TARGET_PLATFORM_SPEC"]

extra_pyqt_configuration = [
"PyQt_qreal_double", "PyQt_OpenSSL", "PyQt_RawFont",
"PyQt_BearerManagement" # This may be needed if not detected properly.
]
