PyQt4 for Embedded Linux Patches
================================

:Author: `David Boddie`_
:Date: %(date)s
:Version: %(version)s

.. _`David Boddie`:             mailto:david@boddie.org.uk

*Note: This text is marked up using reStructuredText formatting. It should be
readable in a text editor but can be processed to produce versions of this
document in other formats.*


.. contents::


Introduction
------------

This package contains a set of patches for Python, Qt, SIP and PyQt4 that
enable PyQt4 to be built for use with Qt for Embedded Linux, a version of
Qt that uses the Linux framebuffer instead of an X server for rendering
graphical user interfaces.

The process of creating a suitable PyQt4 modules is performed by an
installation script and a set of configuration files. In this process, versions
of Python and SIP are built for the build (host) machine. These are used to
configure and build equivalent versions of Python, SIP, Qt and PyQt4 for the
target machine using a cross-compiler.

Since the location of compilers and cross-compilers will be different on each
user's system, the user will need to adapt one of the configuration files to
describe their tools and environment. This information is used by the
installation script to correctly configure and build each component, and to
install it in the correct location.


Configuration
-------------

*Note:* If you have obtained this software directly from its repository, you
first need to create patch archives. Instructions for this are described in
the `Creating Patch Archives`_ section later in this document.

The `Settings` directory contains a collection of configuration files that
describe local settings, such as the location of cross-compilers, and
information about different profiles of Qt. One of these will need to be
customised with the appropriate values for your system.


Required Packages
-----------------

You need to obtain archives for Python, zlib, SIP, Qt and PyQt4. At the time
of writing, you can find the latest versions of the required packages at the
following locations:

Python (use the minor version you prefer):
  https://www.python.org/ftp/python/2.7.13/Python-2.7.13.tgz
zlib:
  http://www.zlib.net/zlib-1.2.11.tar.gz
Qt:
  http://download.qt.io/archive/qt/4.8/4.8.6/qt-everywhere-opensource-src-4.8.6.tar.gz
SIP:
  https://sourceforge.net/projects/pyqt/files/sip/sip-4.19.2/sip-4.19.2.tar.gz
PyQt4:
  http://sourceforge.net/projects/pyqt/files/PyQt4/PyQt-4.12/PyQt4_gpl_x11-4.12.tar.gz

Later versions of these may work. Earlier versions may cause problems with the
patching mechanism used by the `install.py` program.

Recently, only Python 2.6 has been tested. Earlier versions of the patches
worked with Python 2.5:

  http://www.python.org/ftp/python/2.5.4/Python-2.5.4.tar.bz2

Before Qt 4.8 became available, the patches were used with Qt 4.7 and PyQt 4.8:

  http://download.qt.io/archive/qt/4.7/qt-everywhere-opensource-src-4.7.4.tar.gz
  http://www.riverbankcomputing.co.uk/static/Downloads/sip4/sip-4.11.2.tar.gz
  http://www.riverbankcomputing.co.uk/static/Downloads/PyQt4/PyQt-x11-gpl-4.8.tar.gz

Before Qt 4.7 became available, the patches were used with Qt 4.6 and PyQt 4.7:

  http://download.qt.io/archive/qt/4.6/qt-everywhere-opensource-src-4.6.4.tar.gz
  http://www.riverbankcomputing.co.uk/static/Downloads/sip4/sip-4.11.1.tar.gz
  http://www.riverbankcomputing.co.uk/static/Downloads/PyQt4/PyQt-x11-gpl-4.7.6.tar.gz

As mentioned, the current patches may work with these older components, but
they have not been tested recently.

You will also need to install some tools and compilers. On Debian systems, the
following packages are required:

  build-essential python-dev mercurial python-docutils

You will also need to install cross-compilers, such as those from the
`g++-arm-linux-gnueabihf` package.


Optional Packages
-----------------

Some packages are optional. If these are located by the installation script,
and the build process requires them, they will be built as well.

tslib:
  http://github.com/kergoth/tslib
  http://prdownload.berlios.de/tslib/tslib-1.0.tar.bz2
libtool (required for tslib):
  http://www.gnu.org/software/libtool/

If you want to use tslib to handle touchscreen input, you will need to download
the source code for this library as well.

tslib requires libtool as part of the build process so, if you choose to
build tslib, you need to find a suitable version of this and install it on your
system.

The development version of tslib, available from GitHub, contains changes that
fix plugin loading, so creating an archived unofficial version of this for use
with the install.py script is preferred. You should take a snapshot of the
sources from the git repository and give the archive a file name of
tslib-1.1-unofficial.tar.gz or similar. Later released versions of tslib should
contain the necessary fixes.


Building and Installing
-----------------------

*Note:* If you have obtained this software directly from its repository, you
first need to create patch archives. Instructions for this are described in
the `Creating Patch Archives`_ section later in this document.

To begin with, you need to copy a suitable settings file from the `Settings`
directory and change it to describe the architecture of the host (desktop) and
target (device) systems, the location of cross-compilers, and the installation
directory for the Python, SIP, Qt and PyQt packages. Examine each value
carefully as many of the interdependencies between them have not yet been
factored out of the configuration file.

To make things simple, place your new configuration file in the directory
alongside the `install.py` file and rename it to `local_settings.py`.

The old installation process (see below) involved cross-compiling a version of
Python. This is fragile and became a lot more effort than I liked beyond a
certain point. Since I use a Debian system, I now base builds on the Python
binaries that Debian make available. As a result, you should be able to check
that the HOST_PLATFORM entry in the `local_settings.py` file matches your
system's architecture (run `dpkg --print-architecture` to see this) and run the
`deb_install.py` tool::

  ./deb_install.py

Use the `-q` option if you don't want to be asked about the configuration.

Old Installation Process
~~~~~~~~~~~~~~~~~~~~~~~~

This subsection describes the original installation process that build a Python
interpreter for the target platform. This doesn't work for the most recent
versions of Python 2, though it may be fixed to work again in the future.

Once you are happy with your chosen settings, enter the directory containing
the `install.py` file in a terminal and enter the following at the command
line::

  ./install.py local_settings.py

The program will display the configuration and ask you whether you want to
continue or exit. If you are happy with the configuration, enter `c` to
continue.

At this point, the program may exit, asking you to locate one or more of the
source archives mentioned above and place them in the current directory. Once
this has been done, run `install.py` again with the `-q` option to skip the
"Continue or exit" question::

  ./install.py local_settings.py -q

The build should now begin, and will take some time to complete. Currently, the
build system does not record how much it has done, so it is not designed to be
interrupted and resumed again later. However, you can interrupt it and resume
later if you know which actions have been performed. Just comment out the
appropriate function calls starting with `build_` near the end of the
`install.py` file.


Creating Patch Archives
-----------------------

The build system requires a set of patch archives to be present. These contain
patches for each of the components that will be be built and installed. When
this software has been obtained directly from its source repository, the main
directory only contains a collection of subdirectories alongside the
`install.py` file.

To create a collection of patch archives, enter the main directory (the
directory containing the `install.py` file) and run the `make-packages.py`
script from the `Tools` directory with the version numbers of the Python, Qt
and PyQt archives you intend to use, along with the version number for the
package as a whole; for example::

  ./Tools/make-packages.py 2.6 4.7 4.8 1.0

This will generate a directory called `Python-2.6-Qt-4.7-PyQt-4.8-patches-1.0`
and create a corresponding archive, ready to be deployed. You can now enter
the newly-created directory and follow the instructions in the previous
sections to build and install the software.


Licenses for Derived Works
--------------------------

Different portions of this package are licensed under two different types of
license.

Patches to Qt 4, SIP and PyQt4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Patches to Qt 4, SIP and PyQt4 are licensed under the GNU General Public License
(version 3 or later)::

 Patches for Qt 4, SIP and PyQt4 to enable cross-compilation and
 reduced-feature builds.
 Copyright (C) 2007-2010 David Boddie <david@boddie.org.uk>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

Patches to Python 2.5 and Python 2.6
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Patches to Python 2.5 and Python 2.6 are licensed under the Python Software
Foundation License Version 2::

 Patches for Python 2.5 and Python 2.6 to enable cross-compilation and
 reduced-feature builds.
 Copyright (C) 2007-2010 David Boddie <david@boddie.org.uk>

 1. This LICENSE AGREEMENT is between the Python Software Foundation
 ("PSF"), and the Individual or Organization ("Licensee") accessing and
 otherwise using this software ("Python") in source or binary form and
 its associated documentation.

 2. Subject to the terms and conditions of this License Agreement, PSF hereby
 grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
 analyze, test, perform and/or display publicly, prepare derivative works,
 distribute, and otherwise use Python alone or in any derivative version,
 provided, however, that PSF's License Agreement and PSF's notice of copyright,
 i.e., "Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009 Python
 Software Foundation; All Rights Reserved" are retained in Python alone or in any
 derivative version prepared by Licensee.

 3. In the event Licensee prepares a derivative work that is based on
 or incorporates Python or any part thereof, and wants to make
 the derivative work available to others as provided herein, then
 Licensee hereby agrees to include in any such work a brief summary of
 the changes made to Python.

 4. PSF is making Python available to Licensee on an "AS IS"
 basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND
 DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON WILL NOT
 INFRINGE ANY THIRD PARTY RIGHTS.

 5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
 A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON,
 OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.

 6. This License Agreement will automatically terminate upon a material
 breach of its terms and conditions.

 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between PSF and
 Licensee.  This License Agreement does not grant permission to use PSF
 trademarks or trade name in a trademark sense to endorse or promote
 products or services of Licensee, or any third party.

 8. By copying, installing or otherwise using Python, Licensee
 agrees to be bound by the terms and conditions of this License
 Agreement.


Patches to Python 2.7
---------------------

The Python-2.7-embedded-patches/Python-2.7.3-xcompile.patch file was obtained
from the following site and modified to disable ctypes support and installation
of certain scripts.

http://randomsplat.com/wp-content/uploads/2012/10/Python-2.7.3-xcompile.patch

Licenses for Additional Tools
-----------------------------

The tools and modules supplied in the `Tools` directory are licensed under the
GNU General Public License (version 3 or later)::

 Copyright (C) 2007-2010 David Boddie <david@boddie.org.uk>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
