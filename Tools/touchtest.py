#!/usr/bin/env python

import sys
from PyQt4.QtCore import Qt
from PyQt4.QtGui import *

class Window(QWidget):

    def __init__(self, parent = None):
    
        QWidget.__init__(self, parent)
        
        self.label = QLabel()
        self.label.setText("Touch the screen")
        self.label.setAlignment(Qt.AlignCenter)
        layout = QVBoxLayout()
        layout.addWidget(self.label)
        self.setLayout(layout)
    
    def mousePressEvent(self, event):
    
        pos = event.globalPos()
        self.label.setText("(%i, %i)" % (pos.x(), pos.y()))

if __name__ == "__main__":

    app = QApplication(sys.argv)
    w = Window()
    w.showFullScreen()
    sys.exit(app.exec_())
