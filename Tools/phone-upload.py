#!/usr/bin/env python

# phone-upload.py - Uploads files to the Greenphone via FTP.
# Copyright (C) 2007 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ftplib, os, sys

if __name__ == "__main__":

    if len(sys.argv) < 2:
        sys.stderr.write("Usage: %s <files>\n" % sys.argv[0])
        sys.exit(1)
    
    ip_address = os.getenv("GREENPHONE_IP")
    if not ip_address:
        ip_address = "10.10.10.20"
    
    ftp = ftplib.FTP(ip_address)
    ftp.login(user = "anonymous", passwd = "")
    
    try:
        ftp.cwd("uploads")
    except ftplib.error_perm:
        sys.stderr.write("Failed to enter the uploads directory.\n")
        sys.exit(1)
    
    for path in sys.argv[1:]:
    
        name = os.path.split(path)[1]
        try:
            print ftp.storbinary("STOR %s" % name, open(path, "rb"))
        except ftplib.error_perm:
            sys.stderr.write("Failed to upload file: %s\n" % path)
    
    ftp.close()
    sys.exit()
