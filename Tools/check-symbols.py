#!/usr/bin/env python

import commands, os, subprocess, sys

if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.stderr.write("Usage: %s <objdump executable> <Qt library> <PyQt module>\n" % sys.argv[0])
        sys.exit(1)
    
    objdump = sys.argv[1]
    qt_lib = sys.argv[2]
    pyqt_mod = sys.argv[3]
    
    s = subprocess.Popen([objdump, "-T", qt_lib], stdout = subprocess.PIPE)
    qt_symbols = set()
    for line in s.stdout.readlines():
        ls = line.strip().split()
        if ls and ls[-2] == "Base":
            qt_symbols.add(ls[-1])
    
    s = subprocess.Popen([objdump, "-T", pyqt_mod], stdout = subprocess.PIPE)
    pyqt_symbols = []
    for line in s.stdout.readlines():
        ls = line.strip().split()
        if ls and ls[-3] == "*UND*":
            pyqt_symbols.append(ls[-1])
    
    missing = []
    for symbol in pyqt_symbols:
        if symbol.startswith("_") and symbol not in qt_symbols:
            missing.append(symbol)
    
    missing.sort()
    for symbol in missing:
        os.system("c++filt " + commands.mkarg(symbol))
