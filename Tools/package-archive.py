#!/usr/bin/env python

# package-archive.py - Generates archives for deployment, based on file lists.
# Copyright (C) 2007 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, shutil, sys

def create_directory(directory):

    parent, child = os.path.split(directory)
    if not os.path.exists(parent):
        create_directory(parent)
    os.mkdir(directory)


if __name__ == "__main__":

    if len(sys.argv) != 4:
    
        sys.stderr.write("Usage: %s <package list> <destination directory> <archive name>\n" % sys.argv[0])
        sys.exit(1)
    
    package_list = sys.argv[1]
    destination = sys.argv[2]
    archive_name = sys.argv[3]
    links = []
    
    if not os.path.exists(destination):
        os.mkdir(destination)
        created = True
    else:
        created = False
    
    for line in open(package_list).xreadlines():
    
        line = line.rstrip()
        if line == "":
            continue
        
        if line.startswith("<- "):
            links.append(line[3:])
            continue
        
        print line
        
        destination_path = os.path.join(destination, line)
        directory, filename = os.path.split(destination_path)
        
        if not os.path.exists(directory):
            create_directory(directory)
        
        shutil.copy2(line, destination_path)
        
        if links:
            for link in links:
                print "Linking %s to %s." % (link, line)
                os.system("cd %s; ln -s %s %s" % (destination, line, link))
            links = []
    
    if archive_name.endswith(".tar.gz"):
        os.system("tar zcf " + archive_name + " " + destination)
    elif archive_name.endswith(".tar.bz2"):
        os.system("tar jcf " + archive_name + " " + destination)
    else:
        sys.stderr.write("Could not create "+archive_name+"\n")
        sys.exit(1)
    
    if created:
        shutil.rmtree(destination)
    
    sys.exit()
