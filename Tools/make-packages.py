#!/usr/bin/env python

# make-packages.py - Generates patch archives for Qt 4, SIP, Python and PyQt4.
# Copyright (C) 2009-2010 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This tool generates a set of archives containing for Qt 4, SIP, Python and
PyQt4, enabling cross-compilation and reduced-feature builds."""

import os, sys, time

def chdir(path):
    os.chdir(path)
    print "Entered", path, "directory."
    print

def mkdir(path, dir = None):

    if dir:
        path = os.path.join(path, dir)
    if not os.path.exists(path):
        os.mkdir(path)
        print "Created", path, "directory."

def rmdir(path, dir = None):

    if dir:
        path = os.path.join(path, dir)
    if not os.path.exists(path):
        os.rmdir(path)
        print "Removed", path, "directory."

def system(command, verbose = True):

    if verbose:
        print command
    if os.system(command):
        sys.exit(1)


if __name__ == "__main__":

    if len(sys.argv) != 5:
    
        sys.stderr.write("Usage: %s <Python version> <Qt version> <PyQt version> <this package version>\n" % sys.argv[0])
        sys.exit(1)
    
    python_version = sys.argv[1]
    qt_version = sys.argv[2]
    pyqt_version = sys.argv[3]
    this_version = sys.argv[4]
    package_name = "Python-%s-Qt-%s-PyQt-%s-patches-%s" % (python_version, qt_version, pyqt_version, this_version)
    
    mkdir(package_name)
    documents_path = os.path.join(package_name, "Documents")
    mkdir(documents_path)
    settings_path = os.path.join(package_name, "Settings")
    mkdir(settings_path)
    tools_path = os.path.join(package_name, "Tools")
    mkdir(tools_path)
    
    system("cp -p install.py " + os.path.join(package_name, "install.py"))
    system("cp -p deb_install.py " + os.path.join(package_name, "deb_install.py"))
    readme_text = open("README.txt").read()
    system("cp -rp Documents/*.txt " + documents_path)
    system("cp -rp Settings/*.py " + settings_path)
    system("cp -rp Tools/*.py " + tools_path)
    
    system("cp -rp PyQt4-Qt-Embedded-Linux-patches " + os.path.join(package_name, "PyQt4-Qt-Embedded-Linux-%s-patches" % pyqt_version))
    system("cp -rp sip-Qt-Embedded-Linux-changes " + os.path.join(package_name, "sip-Qt-Embedded-Linux-%s-changes" % qt_version))
    system("cp -rp qt-embedded-linux-patches " + os.path.join(package_name, "qt-embedded-linux-%s-patches" % qt_version))
    if os.path.isdir("Python-%s-embedded-patches" % python_version):
        system("cp -rp " + ("Python-%s-embedded-patches" % python_version) + " " + os.path.join(package_name, "Python-%s-embedded-patches" % python_version))
    else:
        system("cp -rp Python-embedded-patches " + os.path.join(package_name, "Python-%s-embedded-patches" % python_version))
    
    # Copy the license files into the package directory.
    system("cp -p COPYING.GPL3 " + os.path.join(package_name, "COPYING.GPL3"))
    system("cp -p LICENSE.PYTHON " + os.path.join(package_name, "LICENSE.PYTHON"))
    
    chdir(package_name)
    
    system("tar zcf PyQt4-Qt-Embedded-Linux-%s-patches.tar.gz PyQt4-Qt-Embedded-Linux-%s-patches" % (pyqt_version, pyqt_version))
    system("tar zcf sip-Qt-Embedded-Linux-%s-changes.tar.gz sip-Qt-Embedded-Linux-%s-changes" % (qt_version, qt_version))
    system("tar zcf qt-embedded-linux-%s-patches.tar.gz qt-embedded-linux-%s-patches" % (qt_version, qt_version))
    system("tar zcf Python-%s-embedded-patches.tar.gz Python-%s-embedded-patches" % (python_version, python_version))
    
    system("rm -rf PyQt4-Qt-Embedded-Linux-%s-patches" % pyqt_version)
    system("rm -rf sip-Qt-Embedded-Linux-%s-changes" % qt_version)
    system("rm -rf qt-embedded-linux-%s-patches" % qt_version)
    system("rm -rf Python-%s-embedded-patches" % python_version)
    
    readme_text = readme_text % {
        "version": this_version,
        "date": time.strftime("%Y-%m-%d", time.gmtime(time.time()))
        }
    open("README.txt", "w").write(readme_text)
    
    
    chdir(os.pardir)
    
    system("tar zcf %s.tar.gz %s" % (package_name, package_name))
    
    sys.exit()
