#!/usr/bin/env python

# serial_tool.py - Experimental functions for file transfer via a serial console.
# Copyright (C) 2009 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64, md5, os, serial, sys


class Session:

    def __init__(self, port, baudrate):
    
        self.port = port
        self.baudrate = baudrate
    
    def read_paragraph(self):
    
        lines = []
        while True:
            line = self.serial.readline()
            if line == "\r\n":
                break
            elif not line:
                break
            else:
                lines.append(line)
        return lines
    
    def read_blank_lines(self):
    
        while self.serial.readline() == "\r\n":
            pass
    
    def print_paragraph(self):
    
        lines = []
        while True:
            line = self.serial.readline()
            print repr(line)
            if line == "\r\n":
                break
            elif not line:
                break
            else:
                lines.append(line)
        return lines
        
    def print_blank_lines(self):
    
        while True:
            line = self.serial.readline()
            print repr(line)
            if line != "\r\n":
                break
    
    def get(self, remote_path, path = None):
    
        if not path:
            path = remote_path
        
        self.serial.write('python -c "'
                'import base64, sys; '
                'data = base64.encodestring('
                    'open(\\"%s\\").read()'
                    '); '
                'sys.stdout.write('
                    '(\'### Begin (%%i bytes) ###\\n\' %% len(data)) '
                    '+ data)"\r\n' % remote_path)
        
        while True:
            line = self.serial.readline()
            if line.startswith("### Begin"):
                length = int(line.split()[2][1:])
                break
        
        i = 0
        lines = []
        while i < length:
            line = self.serial.readline().replace("\r", "")
            lines.append(line)
            i += len(line)
            sys.stdout.write("\r%s   %i%%" % (remote_path, (100*i)/length))
            sys.stdout.flush()
        
        data = "".join(lines)
        raw_data = base64.decodestring(data)
        
        # Read the rest of the echoed input lines.
        self.read_paragraph()
        
        # Check that the file was transferred correctly.
        self.serial.write('md5sum %s\r\n' % remote_path)
        data = ""
        while True:
            data += self.serial.readline().rstrip()
            if data.endswith(remote_path):
                break
        
        value = self.serial.readline()
        a, b = value.split()
        
        # Read the prompt.
        self.serial.readline()
        
        m = md5.md5()
        m.update(raw_data)
        
        digest_ok = a == m.hexdigest()
        path_ok = b == remote_path
        successful = digest_ok and path_ok
        
        if successful:
            sys.stdout.write("   OK\n")
            open(path, "wb").write(raw_data)
        elif not digest_ok:
            sys.stdout.write("   (transfer error)\n")
            sys.stdout.write("Local:  %s\n" % m.hexdigest())
            sys.stdout.write("Remote: %s\n" % a)
        else:
            sys.stdout.write("   (?)\n")
        
        return successful
    
    def put(self, path, remote_path = None):
    
        if not remote_path:
            remote_path = path
        
        self.serial.write('python -c "'
                'import base64, sys; '
                'open(\\"%s\\", \\"w\\").write('
                    'base64.decodestring(sys.stdin.read())'
                    ')"\r\n' % remote_path)
        
        raw_data = open(path, "rb").read()
        data = base64.encodestring(raw_data)
        lines = data.split("\n")
        i = 0
        length = len(lines)
        
        for line in lines:
            self.serial.write(line + "\n")
            line = self.serial.readline()
            i += 1
            sys.stdout.write("\r%s   %i%%" % (remote_path, (100*i)/length))
            sys.stdout.flush()
        
        # Read the rest of the echoed input lines.
        self.read_paragraph()
        
        self.serial.write("\x04\r")

        # Read blank lines and the prompt.
        self.read_blank_lines()
        
        # Check that the file was transferred correctly.
        self.serial.write('md5sum %s\r\n' % remote_path)
        data = ""
        while True:
            data += self.serial.readline().rstrip()
            if data.endswith(remote_path):
                break
        
        value = self.serial.readline()
        a, b = value.split()
        
        # Read the prompt.
        self.serial.readline()
        
        m = md5.md5()
        m.update(raw_data)
        
        digest_ok = a == m.hexdigest()
        path_ok = b == remote_path
        successful = digest_ok and path_ok
        
        if successful:
            sys.stdout.write("   OK\n")
        elif not digest_ok:
            sys.stdout.write("   (transfer error)\n")
            sys.stdout.write("Local:  %s\n" % m.hexdigest())
            sys.stdout.write("Remote: %s\n" % a)
        else:
            sys.stdout.write("   (?)\n")
        
        return successful
    
    def put_files(self, path, remote_path):
    
        self.mkdir(remote_path)
        for obj in os.listdir(path):
        
            child = os.path.join(path, obj)
            remote_child = os.path.join(remote_path, obj)
            
            if os.path.isdir(child):
                self.put_files(child, remote_child)
            else:
                self.put(child, remote_child)
    
    def list(self, arguments = ""):
    
        self.serial.write("ls %s\r\n" % arguments)
        
        lines = []
        while True:
            line = self.serial.readline()
            if not line:
                break
            lines.append(line)
        
        sys.stdout.write("".join(lines[:-1]))
    
    def mkdir(self, remote_path):
    
        self.serial.write("mkdir %s\r\n" % remote_path)
        self.read_paragraph()
    
    def run(self, arguments = ""):
    
        self.serial.write(arguments + "\r\n")
        self.read_paragraph()
    
    def setup(self):
    
        self.serial = serial.Serial()
        self.serial.setPort(self.port)
        self.serial.setBaudrate(self.baudrate)
        self.serial.open()
        self.serial.setTimeout(1)


if __name__ == "__main__":

    if len(sys.argv) < 3:
    
        sys.stderr.write("Usage: %s <device> <baud rate>\n" % sys.argv[0])
        sys.exit(1)
    
    session = Session(sys.argv[1], int(sys.argv[2]))
    session.setup()
    
    get = session.get
    put = session.put
    mput = session.put_files
    ls = session.list
    mkdir = session.mkdir
    run = session.run
    s = session.serial
    
    if len(sys.argv) > 3:
    
        command = sys.argv[3]
        if command == "get" and len(sys.argv) == 5:
            get(sys.argv[4])
        elif command == "put" and len(sys.argv) == 5:
            put(sys.argv[4])
        elif command == "mput" and len(sys.argv) == 6:
            mput(sys.argv[4], sys.argv[5])
        elif command == "ls":
            if len(sys.argv) >= 4:
                ls(" ".join(sys.argv[4:]))
            else:
                ls()
        elif command == "mkdir" and len(sys.argv) == 5:
            mkdir(sys.argv[4])
        elif command == "run" and len(sys.argv) > 4:
            run(" ".join(sys.argv[4:]))
    
    #s.close()
    #sys.exit()
