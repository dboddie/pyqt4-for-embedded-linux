#!/usr/bin/env python

# patch_files.py - Patch Qt for Embedded Linux files.
# Copyright (C) 2009 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, glob, shutil, sys


def find_files(pattern, path):

    files = glob.glob(os.path.join(path, pattern))
    
    for dir_name in os.listdir(path):
    
        full_path = os.path.join(path, dir_name)
        
        if os.path.isdir(full_path):
            files += find_files(pattern, full_path)
    
    return files


if __name__ == "__main__":

    if len(sys.argv) != 3:
    
        sys.stderr.write("Usage: %s <diff directory> <target directory>\n" % sys.argv[0])
        sys.exit(1)
    
    patches_dir = os.path.abspath(sys.argv[1])
    diffs = find_files("*"+os.extsep+"diff", patches_dir)
    target_dir = os.path.abspath(sys.argv[2])
    
    target_files = map(lambda path:
        os.path.join(target_dir, path[len(patches_dir+os.sep):path.rfind(os.extsep)]),
        diffs)
    
    for target, diff in zip(target_files, diffs):
    
        print "patch %s %s" % (target, diff)
        if not os.path.exists(target):
            print "%s not found - assuming it does not exist in this version.\n" % target
        if os.system("cp %s %s.old" % (target, target)):
            sys.stderr.write("Failed to preserve file: %s\n" % target)
            sys.exit(1)
        if os.system("patch %s %s" % (target, diff)):
            sys.stderr.write("Failed to patch file: %s\n" % target)
            sys.exit(1)
    
    new_files = find_files("*"+os.extsep+"cpp", patches_dir) + \
                find_files("*"+os.extsep+"h", patches_dir) + \
                find_files("*"+os.extsep+"conf", patches_dir)
    
    for new_file in new_files:
    
        source_file = new_file[len(patches_dir+os.sep):]
        target_path = target_dir
        
        for d in source_file.split(os.sep)[:-1]:
            target_path = os.path.join(target_path, d)
            if not os.path.exists(target_path):
                print "mkdir %s" % target_path
                os.mkdir(target_path)
        
        target = os.path.join(target_dir, source_file)
        print "cp %s %s" % (new_file, target)
        if os.system("cp %s %s" % (new_file, target)):
            sys.stderr.write("Failed to preserve file: %s\n" % target)
            sys.exit(1)
    
    sys.exit()
