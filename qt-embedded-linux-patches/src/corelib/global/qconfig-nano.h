
/* Data structures */
#ifndef QT_NO_STL
#  define QT_NO_STL
#endif

/* Dialogs */
#ifndef QT_NO_COLORDIALOG
#  define QT_NO_COLORDIALOG
#endif
#ifndef QT_NO_ERRORMESSAGE
#  define QT_NO_ERRORMESSAGE
#endif
#ifndef QT_NO_FONTDIALOG
#  define QT_NO_FONTDIALOG
#endif
#ifndef QT_NO_INPUTDIALOG
#  define QT_NO_INPUTDIALOG
#endif
#ifndef QT_NO_MESSAGEBOX
#  define QT_NO_MESSAGEBOX
#endif
#ifndef QT_NO_FILEDIALOG
#  define QT_NO_FILEDIALOG
#endif
#ifndef QT_NO_PRINTDIALOG
#  define QT_NO_PRINTDIALOG
#endif
#ifndef QT_NO_PROGRESSDIALOG
#  define QT_NO_PROGRESSDIALOG
#endif
#ifndef QT_NO_TABDIALOG
#  define QT_NO_TABDIALOG
#endif

/* Fonts */
#ifndef QT_NO_QWS_QPF
#  define QT_NO_QWS_QPF
#endif

/* Images */
#ifndef QT_NO_IMAGEFORMAT_BMP
#  define QT_NO_IMAGEFORMAT_BMP
#endif
#ifndef QT_NO_IMAGEFORMAT_PPM
#  define QT_NO_IMAGEFORMAT_PPM
#endif
#ifndef QT_NO_IMAGEFORMAT_XBM
#  define QT_NO_IMAGEFORMAT_XBM
#endif
#ifndef QT_NO_IMAGEFORMAT_XPM
#  define QT_NO_IMAGEFORMAT_XPM
#endif
#ifndef QT_NO_IMAGE_TEXT
#  define QT_NO_IMAGE_TEXT
#endif
#ifndef QT_NO_MOVIE
#  define QT_NO_MOVIE
#endif

/* Internationalization */
#ifndef QT_NO_TEXTCODEC
#  define QT_NO_TEXTCODEC
#endif
#ifndef QT_NO_CODECS
#  define QT_NO_CODECS
#endif
#ifndef QT_NO_TEXTCODECPLUGIN
#  define QT_NO_TEXTCODECPLUGIN
#endif
#ifndef QT_NO_TRANSLATION
#  define QT_NO_TRANSLATION
#endif
#ifndef QT_NO_TRANSLATION_UTF8
#  define QT_NO_TRANSLATION_UTF8
#endif

/* ItemViews */
#ifndef QT_NO_DIRMODEL
#  define QT_NO_DIRMODEL
#endif
#ifndef QT_NO_PROXYMODEL
#  define QT_NO_PROXYMODEL
#endif
#ifndef QT_NO_IDENTITYPROXYMODEL
#  define QT_NO_IDENTITYPROXYMODEL
#endif
#ifndef QT_NO_TABLEVIEW
#  define QT_NO_TABLEVIEW
#endif
#ifndef QT_NO_TREEVIEW
#  define QT_NO_TREEVIEW
#endif

/* Kernel */
#ifndef QT_NO_ACTION
#  define QT_NO_ACTION
#endif
#ifndef QT_NO_CLIPBOARD
#  define QT_NO_CLIPBOARD
#endif
#ifndef QT_NO_CURSOR
#  define QT_NO_CURSOR
#endif
#ifndef QT_NO_DRAGANDDROP
#  define QT_NO_DRAGANDDROP
#endif
#ifndef QT_NO_EFFECTS
#  define QT_NO_EFFECTS
#endif
#ifndef QT_NO_PROPERTIES
#  define QT_NO_PROPERTIES
#endif
#ifndef QT_NO_SESSIONMANAGER
#  define QT_NO_SESSIONMANAGER
#endif
#ifndef QT_NO_SHORTCUT
#  define QT_NO_SHORTCUT
#endif
#ifndef QT_NO_SOUND
#  define QT_NO_SOUND
#endif
#ifndef QT_NO_WHEELEVENT
#  define QT_NO_WHEELEVENT
#endif

/* Networking */
#ifndef QT_NO_COP
#  define QT_NO_COP
#endif
#ifndef QT_NO_NETWORKPROXY
#  define QT_NO_NETWORKPROXY
#endif
#ifndef QT_NO_FTP
#  define QT_NO_FTP
#endif

/* Painting */
#ifndef QT_NO_COLORNAMES
#  define QT_NO_COLORNAMES
#endif
#ifndef QT_NO_DIRECTPAINTER
#  define QT_NO_DIRECTPAINTER
#endif
#ifndef QT_NO_PICTURE
#  define QT_NO_PICTURE
#endif
#ifndef QT_NO_PRINTER
#  define QT_NO_PRINTER
#endif

/* Qtopia Core */
#ifndef QT_NO_QWS_ALPHA_CURSOR
#  define QT_NO_QWS_ALPHA_CURSOR
#endif
#ifndef QT_NO_QWS_DECORATION_STYLED
#  define QT_NO_QWS_DECORATION_STYLED
#endif
#ifndef QT_NO_QWS_DECORATION_WINDOWS
#  define QT_NO_QWS_DECORATION_WINDOWS
#endif
#ifndef QT_NO_QWS_SOUNDSERVER
#  define QT_NO_QWS_SOUNDSERVER
#endif
#ifndef QT_NO_SXE
#  define QT_NO_SXE
#endif
#ifndef QT_NO_QWS_PROPERTIES
#  define QT_NO_QWS_PROPERTIES
#endif

/* Styles */
#ifndef QT_NO_STYLE_MOTIF
#  define QT_NO_STYLE_MOTIF
#endif
#ifndef QT_NO_STYLE_CDE
#  define QT_NO_STYLE_CDE
#endif
#ifndef QT_NO_STYLE_WINDOWS
#  define QT_NO_STYLE_WINDOWS
#endif
#ifndef QT_NO_STYLE_WINDOWSXP
#  define QT_NO_STYLE_WINDOWSXP
#endif

/* Widgets */
#ifndef QT_NO_GROUPBOX
#  define QT_NO_GROUPBOX
#endif
#ifndef QT_NO_LCDNUMBER
#  define QT_NO_LCDNUMBER
#endif
#ifndef QT_NO_DATETIMEEDIT
#  define QT_NO_DATETIMEEDIT
#endif
#ifndef QT_NO_MENU
#  define QT_NO_MENU
#endif
#ifndef QT_NO_MAINWINDOW
#  define QT_NO_MAINWINDOW
#endif
#ifndef QT_NO_DOCKWIDGET
#  define QT_NO_DOCKWIDGET
#endif
#ifndef QT_NO_TOOLBAR
#  define QT_NO_TOOLBAR
#endif
#ifndef QT_NO_MENUBAR
#  define QT_NO_MENUBAR
#endif
#ifndef QT_NO_WORKSPACE
#  define QT_NO_WORKSPACE
#endif
#ifndef QT_NO_PROGRESSBAR
#  define QT_NO_PROGRESSBAR
#endif
#ifndef QT_NO_SIZEGRIP
#  define QT_NO_SIZEGRIP
#endif
#ifndef QT_NO_DIAL
#  define QT_NO_DIAL
#endif
#ifndef QT_NO_TEXTBROWSER
#  define QT_NO_TEXTBROWSER
#endif
#ifndef QT_NO_SPLASHSCREEN
#  define QT_NO_SPLASHSCREEN
#endif
#ifndef QT_NO_STACKEDWIDGET
#  define QT_NO_STACKEDWIDGET
#endif
#ifndef QT_NO_TABWIDGET
#  define QT_NO_TABWIDGET
#endif
#ifndef QT_NO_STATUSBAR
#  define QT_NO_STATUSBAR
#endif
#ifndef QT_NO_STATUSTIP
#  define QT_NO_STATUSTIP
#endif
#ifndef QT_NO_TABLEWIDGET
#  define QT_NO_TABLEWIDGET
#endif
#ifndef QT_NO_TOOLBUTTON
#  define QT_NO_TOOLBUTTON
#endif
#ifndef QT_NO_TABBAR
#  define QT_NO_TABBAR
#endif
#ifndef QT_NO_TOOLBOX
#  define QT_NO_TOOLBOX
#endif
#ifndef QT_NO_WHATSTHIS
#  define QT_NO_WHATSTHIS
#endif
#ifndef QT_NO_TOOLTIP
#  define QT_NO_TOOLTIP
#endif
#ifndef QT_NO_TREEWIDGET
#  define QT_NO_TREEWIDGET
#endif
