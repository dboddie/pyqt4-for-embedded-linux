#!/usr/bin/env python

# deb_install.py - Build and install Qt 4, SIP, Python and PyQt4 for Embedded Linux.
# Copyright (C) 2009-2013 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob, os, sets, shutil, subprocess, sys

################################################################################
# Install dependencies.
################################################################################

# libffi4-dev (ctypes)

################################################################################
# Set up some useful configuration settings.
################################################################################

def read_option(options, argv):

    number = len(argv)
    
    for option in options:
        try:
            argv.remove(option)
        except ValueError:
            pass
    
    return len(argv) != number

################################################################################
# Define useful functions.
################################################################################

def mkdir(path, dir = None):

    if dir:
        path = os.path.join(path, dir)
    if not os.path.exists(path):
        os.mkdir(path)
        print "Created", path, "directory."

def chdir(path):
    os.chdir(path)
    print "Entered", path, "directory."
    print

def system(command, verbose = True):

    if verbose:
        print command
        sys.stdout.flush()
    if os.system(command):
        sys.exit(1)

def prefix_join(*args):

    # Prepend the DESTDIR installation prefix to the absolute path beginning
    # with an absolute prefix path. We need to join these two directly instead
    # of using the os.path module's join() function because that will not
    # join absolute paths.
    dest_dir = env.get("DESTDIR", "")
    prefix_path = os.path.join(*args)
    
    if dest_dir.endswith(os.sep) or prefix_path.startswith(os.sep):
        return dest_dir + prefix_path
    else:
        return dest_dir + os.sep + prefix_path

################################################################################
# Look for archives.
################################################################################

def match_file_name(start, finish, name):

    endings = (os.extsep + "gz", os.extsep + "bz2", os.extsep + "tar")
    allowed = "0123456789."
    
    stem, ext = os.path.splitext(name)
    if ext not in endings:
        return None
    
    for ending in endings:
        if name.endswith(ending):
            name = name[:-len(ending)]
    
    if name.startswith(start) and (finish is None or name.endswith(finish)):
        version = name
        if start:
            version = version[len(start):]
        if finish:
            version = version[:-len(finish)]
        
        for i in range(len(version)):
            if version[i] not in allowed:
                if finish is None:
                    version = version[:i]
                    break
                else:
                    return None
        
        try:
            version = map(int, version.split("."))
        except ValueError:
            return None
        return version
    
    return None

def find_in_list(start, finish, files):

    found = []
    for name in files:
    
        version = match_file_name(start, finish, name)
        if version:
            found.append((name, version))
    
    return found

def find_matching_version(details, target_version):

    for name, version in details:
    
        if version == target_version:
            return name
    
    return details[0][0]

def require_version(name, archive_pattern, minimum, maximum = None):

    sys.stderr.write("Cannot locate an archive for %s.\n" % name)
    sys.stderr.write("Please place a copy of " + archive_pattern % ".".join(map(str, minimum)) + " ")
    if maximum:
        sys.stderr.write("or a version up to " + archive_pattern % ".".join(map(str, maximum)) + " ")
    else:
        sys.stderr.write("or later ")
    sys.stderr.write("in the current working directory.\n")

def choose_version(name, archive_pattern, allowed):

    while len(allowed) > 1:
        print "Please select a version of", name, "to use:"
        i = 1
        for archive, version in allowed:
            print "%i:" % i, archive
            i += 1
        try:
            value = int(raw_input(">"))
        except ValueError:
            continue
        
        if 1 <= value <= len(allowed):
            return [allowed[value - 1]]
    
    return []


def check_version(name, archive_pattern, pairs, minimum, maximum = None, optional = False):

    allowed = []
    for archive, version in pairs:
    
        if maximum and minimum <= version <= maximum:
            allowed.append((archive, version))
        elif minimum <= version:
            allowed.append((archive, version))
        else:
            allowed.append((archive, version))
    
    if not optional and not allowed:
        require_version(name, archive_pattern, minimum, maximum)
    elif len(allowed) > 1:
        allowed = choose_version(name, archive_pattern, allowed)
    
    return allowed


def locate_sources(path = os.curdir):

    global sip_archive, sip_version, sip_changes
    global pyqt_archive, pyqt_changes
    global qt_archive, qt_version, qt_changes
    
    files = os.listdir(path)
    
    sip_archives = find_in_list("sip-", "", files)
    pyqt_archives = find_in_list("PyQt4_gpl_x11-", "", files)
    qt_archives = find_in_list("qt-everywhere-opensource-src-", "", files)
    
    sip_changes = find_in_list("sip-Qt-Embedded-Linux-", "-changes", files)
    pyqt_changes = find_in_list("PyQt4-Qt-Embedded-Linux-", "-patches", files)
    qt_changes = find_in_list("qt-embedded-linux-", "-patches", files)
    
    sip_archives = check_version("SIP", "sip-%s.tar.gz", sip_archives, [4,8,0])
    sip_changes = check_version("SIP changes", "sip-Qt-Embedded-Linux-%s-changes.tar.gz", sip_changes, [4,5,0])
    pyqt_archives = check_version("PyQt", "PyQt4_gpl_x11-%s.tar.gz", pyqt_archives, [4,5,0])
    pyqt_changes = check_version("PyQt patches", "PyQt4-Qt-Embedded-Linux-%s-patches.tar.gz", pyqt_changes, [4,5,0])
    qt_archives = check_version("Qt", "qt-everywhere-opensource-src-%s.tar.bz2", qt_archives, [4,4,0])
    qt_changes = check_version("Qt patches", "qt-embedded-linux-%s-patches.tar.bz2", qt_changes, [4,5])
    
    try:
        sip_archive, sip_version = sip_archives[0]
        sip_changes = find_matching_version(sip_changes, sip_version)
        pyqt_archive = pyqt_archives[0][0]
        pyqt_changes = pyqt_changes[0][0]
        qt_archive, qt_version = qt_archives[0]
        qt_changes = qt_changes[0][0]
    
    except IndexError:
        sys.exit(1)
    
    print "Using SIP from", sip_archive
    print "Using SIP changes from", sip_changes
    print "Using PyQt from", pyqt_archive
    print "Using PyQt patches from", pyqt_changes
    print "Using Qt from", qt_archive
    print "Using Qt patches from", qt_changes
    print

################################################################################
# Make a working directory and enter it.
################################################################################

def make_working_directory():

    if not os.path.exists("Working"):
        os.mkdir("Working")
    
    chdir("Working")

################################################################################
# Unpack archives.
################################################################################

def unpack(archive, unpack_dir = None):

    endings = (os.extsep + "gz", os.extsep + "bz2", os.extsep + "tar")
    
    name = archive
    for ending in endings:
        if name.endswith(ending):
            name = name[:-len(ending)]
    
    unpacked_dir = os.path.abspath(os.path.split(name)[1])
    if unpack_dir:
        unpack_dir = os.path.abspath(unpack_dir)
    
    if unpack_dir:
        if os.path.isdir(unpack_dir):
            print archive, "has already been unpacked to", unpack_dir
            return unpack_dir
    elif os.path.isdir(unpacked_dir):
        print archive, "has already been unpacked to", unpacked_dir
        return unpacked_dir
    
    if archive.endswith(os.extsep + "gz"):
        print "Unpacking", archive
        system("tar zxf "+archive, verbose = False)
    elif archive.endswith(os.extsep + "bz2"):
        print "Unpacking", archive
        system("tar jxf "+archive, verbose = False)
    else:
        sys.stderr.write("Unknown archive file type: %s\n" % archive)
        sys.exit(1)
    
    if unpack_dir:
        unpack_dir = os.path.abspath(unpack_dir)
        print "Moving directory to", unpack_dir
        system("mv "+unpacked_dir + " " + unpack_dir, verbose = False)
        return unpack_dir
    else:
        return unpacked_dir

def unpack_sources():

    global native_sip_dir, target_sip_dir, sip_changes_dir
    global pyqt_dir, pyqt_changes_dir
    global qt_dir, qt_build_dir, qt_changes_dir
    
    native_sip_dir = unpack(
        os.path.join(os.pardir, sip_archive),
        "sip-%s-native" % ".".join(map(str, sip_version))
        )
    target_sip_dir = unpack(
        os.path.join(os.pardir, sip_archive),
        "sip-%s-target" % ".".join(map(str, sip_version))
        )
    sip_changes_dir = unpack(os.path.join(os.pardir, sip_changes))
    
    pyqt_dir = unpack(os.path.join(os.pardir, pyqt_archive))
    pyqt_changes_dir = unpack(os.path.join(os.pardir, pyqt_changes))
    
    qt_dir = unpack(os.path.join(os.pardir, qt_archive))
    
    qt_build_dir = qt_dir + "-build"
    mkdir(qt_build_dir)
    
    qt_changes_dir = unpack(os.path.join(os.pardir, qt_changes))
    
    print
    print "Working directories:"
    print
    print native_sip_dir
    print target_sip_dir
    print sip_changes_dir
    print pyqt_dir
    print pyqt_changes_dir
    print qt_dir
    print qt_build_dir
    print qt_changes_dir
    print

################################################################################
# Update the environment variables.
################################################################################

def update_environment():

    global env
    
    env.update({
        "ORIGINAL_PATH": os.getenv("PATH"),
        "QTDIR": qt_dir,
        "QT_BUILD_DIR": qt_build_dir,
        "QT_VERSION": qt_version,
        "QTPATCHDIR": qt_changes_dir,
        "NATIVE_SIPDIR": native_sip_dir,
        "TARGET_SIPDIR": target_sip_dir,
        "SIPCHANGESDIR": sip_changes_dir,
        "PYQTDIR": pyqt_dir,
        "PYQTPATCHDIR": pyqt_changes_dir,
        "TARGET_TOOLS_BIN": os.path.join(env["TARGET_TOOLS"], "bin"),
        })

def create_target_directories():

    # Create the target directory if necessary.
    target_dir = prefix_join(env["TARGETDIR"])
    
    mkdir(target_dir)
    mkdir(target_dir, "bin")
    mkdir(target_dir, "include")
    mkdir(target_dir, "lib")
    mkdir(target_dir, "man")
    mkdir(target_dir, "share")

def copy_target_python_configuration(env):

    if "PYTHON_DEV_PATH" in env:
        path = env["PYTHON_DEV_PATH"]
    else:
        s = subprocess.Popen("apt-cache show python-dev", stdout=subprocess.PIPE,
                                                          shell=True)
    
        if s.wait() != 0:
            sys.stderr.write("Failed to find information about the python-dev package.\n")
            sys.exit(1)
        
        lines = s.stdout.readlines()
    
        found = filter(lambda line: line.startswith("Depends:"), lines)
        # Depends: name1 (= version1), name2 (= version2), ...
        dev_packages = found[0].split(":")[1].strip()
        # name1 (= version1), name2 (= version2), ...
        for dev_package in dev_packages.split(","):
            dev_package = dev_package.strip()
            if dev_package.startswith("python") and "-dev" in dev_package:
                dev_spec = "lib" + dev_package
                break
        else:
            sys.stderr.write("Failed to find a suitable python-dev dependency.\n")
            sys.exit(1)
        
        # name2 (= version2)
        dev_package = dev_spec.split()[0]
        # name2
        
        s = subprocess.Popen("apt-cache show %s" % dev_package,
                             stdout=subprocess.PIPE, shell=True)
        
        if s.wait() != 0:
            sys.stderr.write("Failed to find information about the python package.\n")
            sys.exit(1)
        
        lines = s.stdout.readlines()
        
        found = filter(lambda line: line.startswith("Filename:"), lines)
        path = found[0].split(":")[1].strip()
        path = path.replace("_"+env["HOST_PLATFORM"], "_"+env["TARGET_ARCH"])
    
    # Fetch and unpack the development, minimal and normal packages.
    fetch_and_unpack_deb(path, "python")
    #fetch_and_unpack_deb("lib" + path.replace("-dev", "-minimal"), "python")
    #fetch_and_unpack_deb(path.replace("-dev", "-all"), "python")
    fetch_and_unpack_deb(path.replace("-dev", ""), "python")
    
    include_src_dirs = os.listdir(os.path.join("python", "usr", "include"))
    include_src_dir = filter(lambda x: x.startswith("python"), include_src_dirs)[0]
    include_dest_dir = os.path.join(env["TARGETDIR"], "include", include_src_dir)
    mkdir(include_dest_dir)
    
    print "Copying configuration files to", include_dest_dir
    src_dir = os.path.join("python", "usr", "include", include_src_dir)
    for name in os.listdir(src_dir):
        shutil.copy2(os.path.join(src_dir, name),
                     os.path.join(include_dest_dir, name))
    
    # Copy the architecture-specific files over those in the general include
    # directory. This works around difficulties telling the sip build system
    # about multiple include directories.
    src_dir = os.path.join("python", "usr", "include", env["TARGET_PLATFORM"], include_src_dir)
    for name in os.listdir(src_dir):
        shutil.copy2(os.path.join(src_dir, name),
                     os.path.join(include_dest_dir, name))
    
    include_dest_dir = os.path.join(env["TARGETDIR"], "include", include_src_dir)
    env["TARGET_PYTHON_INCLUDE_DIR"] = include_dest_dir
    
    src_dir = glob.glob(os.path.join("python", "usr", "lib", include_src_dir, "config*"))[0]
    config_dirname = os.path.split(src_dir)[1]
    
    dest_lib_dir = os.path.join(env["TARGETDIR"], "lib", include_src_dir)
    mkdir(dest_lib_dir)
    config_dest_dir = os.path.join(dest_lib_dir, config_dirname)
    mkdir(config_dest_dir)
    env["TARGET_PYTHON_LIB_DIR"] = dest_lib_dir
    env["TARGET_PYTHON_CONFIG_DIR"] = config_dest_dir
    
    print "Copying configuration files to", config_dest_dir,
    for name in os.listdir(src_dir):
        shutil.copy2(os.path.join(src_dir, name),
                     os.path.join(config_dest_dir, name))
    
    site_dest_path = os.path.join(env["TARGETDIR"], "lib", include_src_dir, "site-packages")
    env["TARGET_PYTHON_SITE_PACKAGES"] = site_dest_path
    system("mkdir -p " + site_dest_path)

def fetch_and_unpack_deb(path, unpack_dir):

    mirror = env.get("DEBIAN_MIRROR", "ftp://ftp.debian.org/debian/")
    url = mirror + path
    local_path = os.path.split(path)[1]
    
    if not os.path.exists(local_path):
    
        print "Downloading", url
        system("wget " + url + " -O " + local_path)
    
    print "Unpacking package", local_path
    try:
        os.mkdir(unpack_dir)
    except OSError:
        pass
    system("dpkg -x " + local_path + " " + unpack_dir)

################################################################################
# Build a native version of SIP.
################################################################################

def build_native_sip():

    # In the SIP directory:
    chdir(env["NATIVE_SIPDIR"])
    
    system("python configure.py")
    system("make -j"+env["HOST_COMPILERS"])

################################################################################
# Build SIP for Embedded Linux.
################################################################################

def build_embedded_sip():

    # Enter the distribution directory.
    chdir(env["TARGET_SIPDIR"])
    
    dest_dir = env.get("DESTDIR", "")
    
    # Copy the specs into the distribution.
    system("cp "+os.path.join(env["SIPCHANGESDIR"], "specs", local_settings.sip_target_platform)+" specs")
    
    env_commands = (
        "PATH="+os.path.join(env["TARGET_TOOLS_BIN"])+":"+env["ORIGINAL_PATH"]+" "
        )
    
    # Trick the sip build system into using a fake distutils.sysconfig module.
    mkdir("distutils")
    open(os.path.join("distutils", "__init__.py"), "w").write("")
    
    print os.path.join("distutils", "sysconfig.py")
    s = open(os.path.join("distutils", "sysconfig.py"), "w")
    s.write("def get_python_lib(*args):\n"
            "  return '%s'\n\n" % env["TARGET_PYTHON_SITE_PACKAGES"])
    s.write("def get_python_inc(prefix=''):\n"
            "  if prefix:\n"
            "    return prefix + '%s'\n"
            "  else:\n"
            "    return '%s'\n\n" % (
                env["TARGET_PYTHON_INCLUDE_DIR"], env["TARGET_PYTHON_INCLUDE_DIR"]))
    s.write("def get_config_h_filename():\n"
            "  return '%s'\n\n" % os.path.join(env["TARGET_PYTHON_INCLUDE_DIR"], "pyconfig.h"))
    s.write("def get_python_lib(*args, **kwargs):\n"
            "  return '%s'\n\n" % env["TARGET_PYTHON_LIB_DIR"])
    s.close()
    
    # Using a native Python interpreter, configure the build system.
    system(
        env_commands + "python" + " configure.py " + \
        "-b "+os.path.join(env["TARGETDIR"], "bin") + " " + \
        "-d "+env["TARGET_PYTHON_SITE_PACKAGES"] + " " + \
        "-e "+env["TARGET_PYTHON_INCLUDE_DIR"] + " " + \
        "-v "+os.path.join(env["TARGETDIR"], "share", "sip") + " " + \
        "-p "+local_settings.sip_target_platform
        )
    
    """config_lines = open("sipconfig.py").readlines()
    cfg = open("sipconfig.py", "w")
    for line in config_lines:
        if "'py_conf_inc_dir'" in line:
            line = "    'py_conf_inc_dir':    '%s'\n" % env["TARGET_PYTHON_INCLUDE_DIR"]
        elif "'py_inc_dir'" in line:
            line = "    'py_inc_dir':         '%s'\n" % env["TARGET_PYTHON_INCLUDE_DIR"]
        elif "'py_lib_dir'" in line:
            line = "    'py_lib_dir':         '%s'\n" % env["TARGET_PYTHON_CONFIG_DIR"]
        cfg.write(line)
    cfg.close()"""
    
    system(env_commands + "make -j"+env["TARGET_COMPILERS"])
    if env.has_key("DESTDIR"):
        system(env_commands + "make install DESTDIR="+env["DESTDIR"])
    else:
        system(env_commands + "make install")

################################################################################
# Build Qt for Embedded Linux.
################################################################################

def read_dependencies(rule, lines, dependencies = {}):

    targets_line = filter(lambda line: line.startswith(rule + ":"), lines)[0]
    targets = filter(lambda t: "-" in t, targets_line.split(":")[1].split())
    
    dependencies.setdefault(rule, {"allow": set(), "deny": set()})
    
    for target in targets:
    
        # Add each dependency of the rule that does not deal with tools,
        # examples, demos or legacy support to a list.
        pieces = target.split("-")
        
        if pieces[1] not in ("tools", "examples", "demos", "qt3support") or \
            pieces[1:3] == ["tools", "bootstrap"]:
        
            # Find the dependencies for each dependency.
            read_dependencies(target, lines, dependencies)
            
            dependencies[rule]["allow"].add(target)
        
        else:
            dependencies[rule]["deny"].add(target)
    
    return dependencies

def filter_dependencies(rule, dependencies, usage = {}):

    if dependencies[rule]["deny"]:
        return False
    
    this_usage = {rule: 1}
    
    for target in dependencies[rule]["allow"]:
        if not filter_dependencies(target, dependencies, this_usage):
            return False
    
    for key, value in this_usage.items():
        usage[key] = usage.get(key, 0) + value
    
    return True

def build_qt(patch = True, qt_options = [], configuration_profile = None, extra_configuration = None):

    # Enter the distribution directory.
    chdir(env["QT_BUILD_DIR"])
    
    dest_dir = env.get("DESTDIR", "")
    
    if patch:
        system("python "+os.path.join(env["QTPATCHDIR"], "patch_files.py") + " " + \
               env["QTPATCHDIR"]+" "+env["QTDIR"])
    
    env_commands = "PATH="+os.path.join(env["TARGET_TOOLS_BIN"])+":"+env["ORIGINAL_PATH"]+" "
    
    command = env_commands + os.path.join(env["QTDIR"], "configure") + \
              " -prefix "+env["TARGETDIR"] + \
              " " + " ".join(qt_options) + \
              " -release -fast -confirm-license"
    
    if configuration_profile:
        command += " -qconfig "+configuration_profile
    if extra_configuration:
        command += " " + "".join(map(lambda x: " -D "+x, extra_configuration))
    
    system(command)
    
    system(env_commands + "make -j"+env["TARGET_COMPILERS"]+" sub-src")
    system(env_commands + "make sub-moc")
    
    # Find the targets for the libraries, tools and plugins we need to install.
    lines = open("Makefile").readlines()
    
    # Construct a tree of dependencies.
    dependencies = read_dependencies("install_subtargets", lines)
    usage = {}
    filtered = filter(lambda dep: filter_dependencies(dep, dependencies, usage),
                      dependencies["install_subtargets"]["allow"])
    
    targets = filter(lambda rule: usage[rule] == 1, filtered)
    
    for target in targets:
    
        if env.has_key("DESTDIR"):
            system(env_commands + "make " + target + " INSTALL_ROOT="+env["DESTDIR"])
        else:
            system(env_commands + "make " + target)
    
    if env.has_key("DESTDIR"):
        system("make install_qmake INSTALL_ROOT="+env["DESTDIR"])
        system("make install_mkspecs INSTALL_ROOT="+env["DESTDIR"])
    else:
        system("make install_qmake")
        system("make install_mkspecs")

################################################################################
# Build PyQt4 for Embedded Linux.
################################################################################

# Qt features with an empty PyQt feature are internal Qt features that should
# have no effect on the PyQt API.

# The following features are PyQt-specific and may need to be passed to
# build_embedded_pyqt() as additional configuration lines:
#
# Py_DateTime
# PyQt_NoQtPrintRangeBug
# PyQt_OpenSSL
# PyQt_qreal_double
# Py_v3

conversion_dict = {
    "QT_NO_ACCESSIBILITY":              "PyQt_Accessibility",
    "QT_NO_ACTION":                     "PyQt_Action",
    "QT_NO_ANIMATION":                  "PyQt_Animation",
    "QT_NO_ANTIALIASING":               "",
    "QT_NO_ARM_EABI":                   "",                     # internal
    "QT_NO_AUDIO_BACKEND":              "",
    "QT_NO_BACKINGSTORE":               "",                     # internal
    "QT_NO_BEARERMANAGEMENT":           "PyQt_BearerManagement",
    "QT_NO_BEZIER":                     "",
    "QT_NO_BIG_CODECS":                 "PyQt_BigCodecs",       # internal
    "QT_NO_BLITTABLE":                  "",
    "QT_NO_BUTTONGROUP":                "PyQt_ButtonGroup",
    "QT_NO_CALENDARWIDGET":             "PyQt_CalendarWidget",
    "QT_NO_CAST_FROM_ASCII":            "",
    "QT_NO_CAST_FROM_BYTEARRAY":        "",
    "QT_NO_CAST_TO_ASCII":              "",
    "QT_NO_CLIPBOARD":                  "PyQt_Clipboard",
    "QT_NO_CLOCK_MONOTONIC":            "",
    "QT_NO_CODECS":                     "PyQt_Codecs",          # internal
    "QT_NO_CODEC_FOR_C_STRINGS":        "",
    "QT_NO_COLORDIALOG":                "PyQt_ColorDialog",
    "QT_NO_COLORNAMES":                 "",
    "QT_NO_COLUMNVIEW":                 "PyQt_ColumnView",
    "QT_NO_COMBOBOX":                   "PyQt_ComboBox",
    "QT_NO_COMPLETER":                  "PyQt_Completer",
    "QT_NO_COMPLEXTEXT":                "",
    "QT_NO_COMPRESS":                   "",
    "QT_NO_CONCURRENT":                 "PyQt_Concurrent",      # not wrapped
    "QT_NO_CONCURRENT_FILTER":          "PyQt_Concurrent_Filter",
    "QT_NO_CONCURRENT_MAP":             "PyQt_Concurrent_Map",
    "QT_NO_CONTEXTMENU":                "PyQt_ContextMenu",
    "QT_NO_COP":                        "PyQt_COP",
    "QT_NO_CPU_FEATURE":                "",
    "QT_NO_CRASHHANDLER":               "",
    "QT_NO_CSSPARSER":                  "PyQt_CSSParser",
    "QT_NO_CUPS":                       "",                     # internal
    "QT_NO_CURSOR":                     "PyQt_Cursor",
    "QT_NO_DATASTREAM":                 "PyQt_DataStream",
    "QT_NO_DATAWIDGETMAPPER":           "PyQt_DataWidgetMapper",
    "QT_NO_DATESTRING":                 "PyQt_DateString",
    "QT_NO_DATETIMEEDIT":               "PyQt_DateTimeEdit",
    "QT_NO_DBUS":                       "PyQt_DBus",
    "QT_NO_DEBUG":                      "",
    "QT_NO_DEBUGSTREAM":                "",
    "QT_NO_DEBUG_OUTPUT":               "",
    "QT_NO_DEBUG_PLUGIN_CHECK":         "",
    "QT_NO_DEBUG_STREAM":               "",
    "QT_NO_DEPRECATED":                 "",
    "QT_NO_DESKTOPSERVICES":            "PyQt_DesktopServices",
    "QT_NO_DIAL":                       "PyQt_Dial",
    "QT_NO_DIALOGBUTTONBOX":            "",
    "QT_NO_DIR":                        "",
    "QT_NO_DIRECTDRAW":                 "",
    "QT_NO_DIRECTFB_CURSOR":            "",
    "QT_NO_DIRECTFB_IMAGECACHE":        "",
    "QT_NO_DIRECTFB_IMAGEPROVIDER":     "",
    "QT_NO_DIRECTFB_IMAGEPROVIDER_KEEPALIVE": "",
    "QT_NO_DIRECTFB_KEYBOARD":          "",
    "QT_NO_DIRECTFB_LAYER":             "",
    "QT_NO_DIRECTFB_MOUSE":             "",
    "QT_NO_DIRECTFB_OPAQUE_DETECTION":  "",
    "QT_NO_DIRECTFB_PALETTE":           "",
    "QT_NO_DIRECTFB_PREALLOCATED":      "",
    "QT_NO_DIRECTFB_STRETCHBLIT":       "",
    "QT_NO_DIRECTFB_SUBSURFACE":        "",
    "QT_NO_DIRECTFB_WINDOW_AS_CURSOR":  "",
    "QT_NO_DIRECTFB_WM":                "",
    "QT_NO_DIRECTPAINTER":              "",                     # internal
    "QT_NO_DIRECTWRITE":                "",
    "QT_NO_DIRMODEL":                   "PyQt_DirModel",
    "QT_NO_DNS":                        "",
    "QT_NO_DOCKWIDGET":                 "PyQt_DockWidget",
    "QT_NO_DOM":                        "PyQt_DOMClasses",
    "QT_NO_DRAGANDDROP":                "PyQt_DragAndDrop",
    "QT_NO_DYNAMIC_CAST":               "",
    "QT_NO_DYNAMIC_LIBRARY":            "",
    "QT_NO_EFFECTS":                    "",                     # internal
    "QT_NO_EGL":                        "",
    "QT_NO_EMIT":                       "",
    "QT_NO_ERRORMESSAGE":               "PyQt_ErrorMessage",
    "QT_NO_EXCEPTIONS":                 "",
    "QT_NO_FAST_MOVE":                  "",
    "QT_NO_FAST_SCROLL":                "",
    "QT_NO_FEATURE":                    "",
    "QT_NO_FILEDIALOG":                 "PyQt_FileDialog",
    "QT_NO_FILEICONPROVIDER":           "",
    "QT_NO_FILESYSTEMITERATOR":         "",
    "QT_NO_FILESYSTEMMODEL":            "PyQt_FileSystemModel",
    "QT_NO_FILESYSTEMWATCHER":          "PyQt_FileSystemWatcher",
    "QT_NO_FONTCOMBOBOX":               "PyQt_FontComboBox",
    "QT_NO_FONTCONFIG":                 "",
    "QT_NO_FONTDIALOG":                 "PyQt_FontDialog",
    "QT_NO_FPU":                        "",
    "QT_NO_FRAME":                      "",
    "QT_NO_FREETYPE":                   "",                     # internal
    "QT_NO_FSCOMPLETER":                "PyQt_FSCompleter",
    "QT_NO_FSFILEENGINE":               "",
    "QT_NO_FTP":                        "PyQt_FTP",
    "QT_NO_GEOM_VARIANT":               "",
    "QT_NO_GESTURES":                   "PyQt_Gestures",
    "QT_NO_GETADDRINFO":                "",
    "QT_NO_GETIFADDRS":                 "",
    "QT_NO_GLIB":                       "",
    "QT_NO_GRAPHICSEFFECT":             "PyQt_GraphicsEffect",
    "QT_NO_GRAPHICSSVGITEM":            "PyQt_GraphicsSVGItem",
    "QT_NO_GRAPHICSVIEW":               "PyQt_GraphicsView",
    "QT_NO_GROUPBOX":                   "PyQt_GroupBox",
    "QT_NO_HOSTINFO":                   "PyQt_HostInfo", # May be missing a test in Qt.
    "QT_NO_HTTP":                       "PyQt_HTTP",
    "QT_NO_ICON":                       "PyQt_Icon", # May be missing a test in Qt.
    "QT_NO_ICONV":                      "",
    "QT_NO_IDENTITYPROXYMODEL":         "PyQt_IdentityProxyModel",
    "QT_NO_IM":                         "PyQt_IM",
    "QT_NO_IMAGEFORMATPLUGIN":          "PyQt_ImageFormatPlugin",
    "QT_NO_IMAGEFORMAT_BMP":            "PyQt_ImageFormat_BMP",
    "QT_NO_IMAGEFORMAT_GIF":            "",
    "QT_NO_IMAGEFORMAT_ICO":            "",
    "QT_NO_IMAGEFORMAT_JPEG":           "PyQt_ImageFormat_JPEG",
    "QT_NO_IMAGEFORMAT_MNG":            "",
    "QT_NO_IMAGEFORMAT_PNG":            "PyQt_ImageFormat_PNG",
    "QT_NO_IMAGEFORMAT_PPM":            "PyQt_ImageFormat_PPM",
    "QT_NO_IMAGEFORMAT_TGA":            "",
    "QT_NO_IMAGEFORMAT_TIFF":           "",
    "QT_NO_IMAGEFORMAT_XBM":            "PyQt_ImageFormat_XBM",
    "QT_NO_IMAGEFORMAT_XPM":            "PyQt_ImageFormat_XPM",
    "QT_NO_IMAGEIO":                    "",
    "QT_NO_IMAGE_COLLECTION_COMPRESSION": "",
    "QT_NO_IMAGE_DITHER_TO_1":          "",
    "QT_NO_IMAGE_HEURISTIC_MASK":       "PyQt_Image_Heuristic_Mask",
    "QT_NO_IMAGE_SMOOTHSCALE":          "",
    "QT_NO_IMAGE_TEXT":                 "PyQt_Image_Text",
    "QT_NO_IMPORT_QT47_QML":            "",
    "QT_NO_IM_PREEDIT_RELOCATION":      "",
    "QT_NO_INOTIFY":                    "",
    "QT_NO_INPUTDIALOG":                "PyQt_InputDialog",
    "QT_NO_INPUTMETHOD":                "",
    "QT_NO_IPV6":                       "",
    "QT_NO_IPV6IFNAME":                 "",
    "QT_NO_ITEMVIEWS":                  "PyQt_ItemViews",
    "QT_NO_KEYWORDS":                   "",
    "QT_NO_LABEL":                      "",
    "QT_NO_LAYOUT":                     "",
    "QT_NO_LCDNUMBER":                  "PyQt_LCDNumber",
    "QT_NO_LIBRARY":                    "PyQt_Library",
    "QT_NO_LINEEDIT":                   "PyQt_LineEdit",
    "QT_NO_LINEEDITMENU":               "PyQt_LineEditMenu", # obsolete in Qt 4.8?
    "QT_NO_LISTVIEW":                   "PyQt_ListView",
    "QT_NO_LISTWIDGET":                 "PyQt_ListWidget",
    "QT_NO_LOCALFILE_OPTIMIZED_QML":    "",
    "QT_NO_LOCALSERVER":                "",
    "QT_NO_LOCALSOCKET":                "",
    "QT_NO_LPR":                        "",
    "QT_NO_MAC_XARCH":                  "",
    "QT_NO_MAINWINDOW":                 "PyQt_MainWindow",
    "QT_NO_MATRIX4X4":                  "",
    "QT_NO_MDIAREA":                    "PyQt_MDIArea",
    "QT_NO_MENU":                       "PyQt_Menu",
    "QT_NO_MENUBAR":                    "PyQt_MenuBar",
    "QT_NO_MESSAGEBOX":                 "PyQt_MessageBox",
    "QT_NO_MIME":                       "",
    "QT_NO_MIMECLIPBOARD":              "",
    "QT_NO_MIMEFACTORY":                "",
    "QT_NO_MITSHM":                     "",
    "QT_NO_MOUSE_INTEGRITY":            "",
    "QT_NO_MOUSE_PC":                   "",
    "QT_NO_MOVIE":                      "PyQt_Movie",
    "QT_NO_MREMAP":                     "",
    "QT_NO_NAS":                        "",
    "QT_NO_NATIVE_GESTURES":            "",
    "QT_NO_NETWORK":                    "",
    "QT_NO_NETWORKDISKCACHE":           "PyQt_NetworkDiskCache",
    "QT_NO_NETWORKINTERFACE":           "",
    "QT_NO_NETWORKPROTOCOL":            "",
    "QT_NO_NETWORKPROTOCOL_FTP":        "",
    "QT_NO_NETWORKPROXY":               "PyQt_NetworkProxy",
    "QT_NO_NIS":                        "",
    "QT_NO_OPENGL":                     "",
    "QT_NO_OPENSSL":                    "PyQt_OpenSSL",
    "QT_NO_OPENTYPE":                   "",
    "QT_NO_PAINTONSCREEN":              "",                     # internal
    "QT_NO_PAINT_DEBUG":                "",                     # internal
    "QT_NO_PDF":                        "",
    "QT_NO_PHONON_ABSTRACTMEDIASTREAM": "",
    "QT_NO_PHONON_AUDIOCAPTURE":        "",
    "QT_NO_PHONON_EFFECT":              "",
    "QT_NO_PHONON_EFFECTWIDGET":        "PyQt_Phonon_EffectWidget",
    "QT_NO_PHONON_MEDIACONTROLLER":     "",
    "QT_NO_PHONON_OBJECTDESCRIPTIONMODEL": "",
    "QT_NO_PHONON_PLATFORMPLUGIN":      "",
    "QT_NO_PHONON_QT_NO_PHONON_MEDIACONTROLLER": "",
    "QT_NO_PHONON_SEEKSLIDER":          "PyQt_Phonon_SeekSlider",
    "QT_NO_PHONON_SETTINGSGROUP":       "PyQt_Phonon_SettingsGroup",
    "QT_NO_PHONON_VIDEO":               "",
    "QT_NO_PHONON_VIDEOPLAYER":         "",
    "QT_NO_PHONON_VOLUMEFADEREFFECT":   "",
    "QT_NO_PHONON_VOLUMESLIDER":        "PyQt_Phonon_VolumeSlider",
    "QT_NO_PICTURE":                    "PyQt_Picture",
    "QT_NO_PRINTDIALOG":                "PyQt_PrintDialog",
    "QT_NO_PICTUREIO":                  "",
    "QT_NO_PLUGIN_CHECK":               "",
    "QT_NO_PRINTER":                    "PyQt_Printer",
    "QT_NO_PRINTPREVIEWDIALOG":         "PyQt_PrintPreviewDialog",
    "QT_NO_PRINTPREVIEWWIDGET":         "PyQt_PrintPreviewWidget",
    "QT_NO_PROCESS":                    "PyQt_Process",
    "QT_NO_PROGRESSBAR":                "PyQt_ProgressBar",
    "QT_NO_PROGRESSDIALOG":             "PyQt_ProgressDialog",
    "QT_NO_PROPERTIES":                 "PyQt_Properties",      # internal
    "QT_NO_PROXYMODEL":                 "PyQt_ProxyModel",
    "QT_NO_QCOLUMNVIEW":                "",
    "QT_NO_QDEBUG_MACRO":               "",
    "QT_NO_QDOCKWIDGET":                "",
    "QT_NO_QFUTURE":                    "PyQt_Future",
    "QT_NO_QOBJECT":                    "",
    "QT_NO_QOBJECT_CHECK":              "",
    "QT_NO_QUATERNION":                 "",
    "QT_NO_QUUID_STRING":               "PyQt_UUIDString",
    "QT_NO_QWARNING_MACRO":             "",
    "QT_NO_QWSEMBEDWIDGET":             "PyQt_QWS_EmbedWidget",
    "QT_NO_QWS_AHI":                    "",
    "QT_NO_QWS_ALPHA_CURSOR":           "",                     # internal
    "QT_NO_QWS_CURSOR":                 "PyQt_QWS_Cursor",
    "QT_NO_QWS_DECORATION_DEFAULT":     "",                     # not wrapped
    "QT_NO_QWS_DECORATION_STYLED":      "",                     # not wrapped
    "QT_NO_QWS_DECORATION_WINDOWS":     "",                     # not wrapped
    "QT_NO_QWS_DIRECTFB":               "",
    "QT_NO_QWS_DYNAMICSCREENTRANSFORMATION": "",                # internal
    "QT_NO_QWS_FSIM":                   "",
    "QT_NO_QWS_INPUTMETHODS":           "PyQt_QWS_InputMethods",
    "QT_NO_QWS_INTEGRITY":              "",
    "QT_NO_QWS_INTEGRITYFB":            "",
    "QT_NO_QWS_KBD_INTEGRITY":          "",
    "QT_NO_QWS_KBD_LINUXINPUT":         "",
    "QT_NO_QWS_KBD_QNX":                "",
    "QT_NO_QWS_KBD_QVFB":               "",
    "QT_NO_QWS_KBD_TTY":                "",
    "QT_NO_QWS_KBD_UM":                 "",
    "QT_NO_QWS_KEYBOARD":               "PyQt_QWS_Keyboard",
    "QT_NO_QWS_LINUXFB":                "",
    "QT_NO_QWS_MANAGER":                "PyQt_QWS_Manager",
    "QT_NO_QWS_MOUSE":                  "",                     # not wrapped
    "QT_NO_QWS_MOUSE_AUTO":             "",                     # internal
    "QT_NO_QWS_MOUSE_INTEGRITY":        "",
    "QT_NO_QWS_MOUSE_LINUXINPUT":       "",
    "QT_NO_QWS_MOUSE_LINUXTP":          "",
    "QT_NO_QWS_MOUSE_MANUAL":           "",                     # internal
    "QT_NO_QWS_MOUSE_PC":               "",
    "QT_NO_QWS_MOUSE_QNX":              "",
    "QT_NO_QWS_MOUSE_QVFB":             "",
    "QT_NO_QWS_MOUSE_TSLIB":            "",
    "QT_NO_QWS_MULTIPROCESS":           "",                     # internal
    "QT_NO_QWS_MULTISCREEN":            "",
    "QT_NO_QWS_PROPERTIES":             "",                     # internal
    "QT_NO_QWS_PROXYSCREEN":            "",                     # internal
    "QT_NO_QWS_QNX":                    "",
    "QT_NO_QWS_QPF":                    "",                     # internal
    "QT_NO_QWS_QPF2":                   "",                     # internal
    "QT_NO_QWS_QVFB":                   "",
    "QT_NO_QWS_REPEATER":               "",
    "QT_NO_QWS_SHARE_FONTS":            "",
    "QT_NO_QWS_SIGNALHANDLER":          "",
    "QT_NO_QWS_SOUNDSERVER":            "",                     # not wrapped
    "QT_NO_QWS_TRANSFORMED":            "",
    "QT_NO_QWS_VNC":                    "",
    "QT_NO_RASTERCALLBACKS":            "",                     # not wrapped
    "QT_NO_RAWFONT":                    "PyQt_RawFont",         # internal
    "QT_NO_REGEXP":                     "",
    "QT_NO_REGEXP_ANCHOR_ALT":          "",
    "QT_NO_REGEXP_BACKREF":             "",
    "QT_NO_REGEXP_CAPTURE":             "",
    "QT_NO_REGEXP_CCLASS":              "",
    "QT_NO_REGEXP_ESCAPE":              "",
    "QT_NO_REGEXP_INTERVAL":            "",
    "QT_NO_REGEXP_LOOKAHEAD":           "",
    "QT_NO_REGEXP_OPTIM":               "",
    "QT_NO_REGEXP_WILDCARD":            "",
    "QT_NO_RESIZEHANDLER":              "",                     # internal
    "QT_NO_RUBBERBAND":                 "PyQt_RubberBand",
    "QT_NO_SCRIPT":                     "",             # internal/unnecessary
    "QT_NO_SCROLLAREA":                 "PyQt_ScrollArea",
    "QT_NO_SCROLLBAR":                  "PyQt_ScrollBar",
    "QT_NO_SEMAPHORE":                  "",
    "QT_NO_SESSIONMANAGER":             "PyQt_SessionManager",
    "QT_NO_SETLOCALE":                  "",
    "QT_NO_SETTINGS":                   "PyQt_Settings",
    "QT_NO_SETTINGSGROUP":              "",
    "QT_NO_SHAREDMEMORY":               "PyQt_SharedMemory",
    "QT_NO_SHORTCUT":                   "PyQt_Shortcut",
    "QT_NO_SIGNALMAPPER":               "PyQt_SignalMapper",
    "QT_NO_SIZEGRIP":                   "PyQt_SizeGrip",
    "QT_NO_SLIDER":                     "PyQt_Slider",
    "QT_NO_SOCKS5":                     "",                     # internal
    "QT_NO_SOFTKEYMANAGER":             "PyQt_SoftKeyManager",
    "QT_NO_SORTFILTERPROXYMODEL":       "PyQt_SortFilterProxyModel",
    "QT_NO_SOUND":                      "PyQt_Sound",
    "QT_NO_SPINBOX":                    "PyQt_SpinBox",
    "QT_NO_SPLASHSCREEN":               "PyQt_SplashScreen",
    "QT_NO_SPLITTER":                   "PyQt_Splitter",
    "QT_NO_SQL":                        "",
    "QT_NO_SQL_EDIT_WIDGETS":           "",
    "QT_NO_SQL_FORM":                   "",
    "QT_NO_SQL_VIEW_WIDGETS":           "",
    "QT_NO_STACKEDWIDGET":              "PyQt_StackedWidget",
    "QT_NO_STANDARDITEMMODEL":          "PyQt_StandardItemModel",
    "QT_NO_STATEMACHINE":               "PyQt_StateMachine",
    "QT_NO_STATEMACHINE_EVENTFILTER":   "",
    "QT_NO_STATUSBAR":                  "PyQt_StatusBar",
    "QT_NO_STATUSTIP":                  "PyQt_StatusTip",
    "QT_NO_STL":                        "",                     # internal
    "QT_NO_STL_WCHAR":                  "",
    "QT_NO_STRINGLISTMODEL":            "PyQt_StringListModel",
    "QT_NO_STYLE_CDE":                  "",                     # not wrapped
    "QT_NO_STYLE_CLEANLOOKS":           "",                     # not wrapped
    "QT_NO_STYLE_MOTIF":                "",                     # not wrapped
    "QT_NO_STYLE_PLASTIQUE":            "",                     # not wrapped
    "QT_NO_STYLE_PROXY":                "",
    "QT_NO_STYLE_QGTK":                 "",
    "QT_NO_STYLE_STYLESHEET":           "PyQt_Style_StyleSheet",
        # Beware: QApplication::styleSheet() is not covered.
    "QT_NO_STYLE_WINDOWS":              "",                     # not wrapped
    "QT_NO_STYLE_WINDOWSCE":            "",                     # not wrapped
    "QT_NO_STYLE_WINDOWSMOBILE":        "",
    "QT_NO_STYLE_WINDOWSXP":            "",
    "QT_NO_STYLE_WINDOWSVISTA":         "",
    "QT_NO_SUBTRACTOPAQUESIBLINGS":     "",
    "QT_NO_SVG":                        "",             # internal/unnecessary
    "QT_NO_SVGGENERATOR":               "PyQt_SVGGenerator",
    "QT_NO_SVGRENDERER":                "PyQt_SVGRenderer",
    "QT_NO_SVGWIDGET":                  "PyQt_SVGWidget",
    "QT_NO_SXE":                        "",                     # internal
    "QT_NO_SYNTAXHIGHLIGHTER":          "PyQt_SyntaxHighlighter",
    "QT_NO_SYSTEMLOCALE":               "",
    "QT_NO_SYSTEMSEMAPHORE":            "PyQt_SystemSemaphore",
    "QT_NO_SYSTEMTRAYICON":             "PyQt_SystemTrayIcon",
    "QT_NO_TABBAR":                     "PyQt_TabBar",
    "QT_NO_TABDIALOG":                  "",                     # internal
    "QT_NO_TABLETEVENT":                "PyQt_TabletEvent",
    "QT_NO_TABLEVIEW":                  "PyQt_TableView",
    "QT_NO_TABLEWIDGET":                "PyQt_TableWidget",
    "QT_NO_TABWIDGET":                  "PyQt_TabWidget",
    "QT_NO_TEMPLATE_TEMPLATE_PARAMETER": "",
    "QT_NO_TEMPLATE_TEMPLATE_PARAMETERS": "",
    "QT_NO_TEMPORARYFILE":              "PyQt_TemporaryFile",
    "QT_NO_TEXTBROWSER":                "PyQt_TextBrowser",
    "QT_NO_TEXTCODEC":                  "PyQt_TextCodec",
    "QT_NO_TEXTCODECPLUGIN":            "",                     # not wrapped
    "QT_NO_TEXTCONTROL":                "",
    "QT_NO_TEXTDATE":                   "PyQt_TextDate",
    "QT_NO_TEXTEDIT":                   "PyQt_TextEdit",
    "QT_NO_TEXTEDITMENU":               "PyQt_TextEditMenu", # obsolete in Qt 4.8?
    "QT_NO_TEXTHTMLPARSER":             "PyQt_TextHTMLParser",
    "QT_NO_TEXTODFWRITER":              "",                     # to do
    "QT_NO_TEXTSTREAM":                 "PyQt_TextStream",
    "QT_NO_THREAD":                     "PyQt_Thread",
    "QT_NO_THREADED_GLIB":              "",
    "QT_NO_TITLEBAR":                   "",
    "QT_NO_TOOLBAR":                    "PyQt_ToolBar",
    "QT_NO_TOOLBOX":                    "PyQt_ToolBox",
    "QT_NO_TOOLBUTTON":                 "PyQt_ToolButton",
    "QT_NO_TOOLTIP":                    "PyQt_ToolTip",
    "QT_NO_TRANSFORMATIONS":            "",
    "QT_NO_TRANSLATION":                "PyQt_Translation",
    "QT_NO_TRANSLATION_BUILDER":        "",
    "QT_NO_TRANSLATION_UTF8":           "",                     # internal
    "QT_NO_TREEVIEW":                   "PyQt_TreeView",
    "QT_NO_TREEWIDGET":                 "PyQt_TreeWidget",
    "QT_NO_UDPSOCKET":                  "PyQt_UDPSocket",
    "QT_NO_UITOOLS":                    "",
    "QT_NO_UNDOCOMMAND":                "PyQt_UndoCommand",
    "QT_NO_UNDOGROUP":                  "PyQt_UndoGroup",
    "QT_NO_UNDOSTACK":                  "PyQt_UndoStack",
        # Beware: QUndoView::setStack() and stack() are not covered.
    "QT_NO_UNDOVIEW":                   "PyQt_UndoView",
    "QT_NO_UNICODE":                    "",
    "QT_NO_UNICODETABLES":              "",
    "QT_NO_UNSETENV":                   "",
    "QT_NO_URL":                        "",
    "QT_NO_URLINFO":                    "PyQt_URLInfo",
    "QT_NO_URL_CAST_FROM_STRING":       "",
    "QT_NO_USERDATA":                   "",
    "QT_NO_USING_NAMESPACE":            "",
    "QT_NO_VALIDATOR":                  "PyQt_Validator",
    "QT_NO_VARIANT":                    "",
    "QT_NO_VECTOR2D":                   "",
    "QT_NO_VECTOR3D":                   "",
    "QT_NO_VECTOR4D":                   "",
    "QT_NO_WARNINGS":                   "",
    "QT_NO_WARNING_OUTPUT":             "",
    "QT_NO_WAYLAND_XKB":                "",
    "QT_NO_WHATSTHIS":                  "PyQt_WhatsThis",
    "QT_NO_WHEELEVENT":                 "PyQt_WheelEvent",
    "QT_NO_WIN_ACTIVEQT":               "",
    "QT_NO_WINDOWGROUPHINT":            "",
    "QT_NO_WIZARD":                     "PyQt_Wizard",
    "QT_NO_WORKSPACE":                  "PyQt_Workspace",
    "QT_NO_WORKSPACEQT_QWS_SCREEN_COORDINATES": "",
    "QT_NO_XMLPATTERNS":                "",
    "QT_NO_XMLSTREAM":                  "PyQt_XMLStream",
    "QT_NO_XMLSTREAMREADER":            "PyQt_XMLStreamReader",
    "QT_NO_XMLSTREAMWRITER":            "PyQt_XMLStreamWriter",
    "QT_QWS_CLIENTBLIT":                "PyQt_QWS_ClientBlit",  # internal
    "QT_QWS_SCREEN_COORDINATES": ""                             # internal
    }

# Features not introduced by Qt need to be passed to build_embedded_pyqt() as additional
# configuration lines.

def read_configuration(configuration_file):

    definitions = sets.Set()
    for line in open(configuration_file).readlines():
    
        pieces = line.strip().split()
        if len(pieces) == 3 and pieces[:2] == ["#", "define"]:
        
            definitions.add(pieces[2])
    
    return list(definitions)

def expand_configuration(configuration_defs, features_file):

    dependents = read_features(features_file)
    definitions = sets.Set()
    
    for defn in configuration_defs:
    
        definitions.add(defn)
        
        features = dependents.get(defn, [])
        i = 0
        while i < len(features):
            definitions.add(features[i])
            features += dependents.get(features[i], [])
            i += 1
    
    return list(definitions)

def read_features(features_file):

    features = {}
    lines = map(lambda line: line.strip(), open(features_file).readlines())
    feature = ""
    
    for line in lines:
    
        try:
            key, value = line.split(":")
        except ValueError:
            continue
        
        if key == "Feature":
            feature = value.lstrip()
        elif feature and key == "Requires" and value:
            features[feature] = value.lstrip().split()
    
    # Turn the dictionary of dependencies into a dictionary of dependents
    # so that we can figure out which features to switch off when any given
    # feature is disabled.
    
    dependents = {}
    for feature, dependencies in features.items():
    
        for dependency in dependencies:
            dependents.setdefault("QT_NO_"+dependency, []).append("QT_NO_"+feature)
    
    return dependents

def convert_configuration(configuration_lines):

    definitions = []
    unknown = []
    for definition in configuration_lines:
    
        try:
            definition = conversion_dict[definition]
        except KeyError:
            unknown.append(definition)
            continue
        
        if definition:
            definitions.append(definition)
    
    if unknown:
        sys.stderr.write("Unknown features: %s\n" % " ".join(unknown))
        sys.exit(1)
    
    definitions.sort()
    
    qt_version_number = (env["QT_VERSION"][0] << 16) + \
                        (env["QT_VERSION"][1] << 8) + \
                        env["QT_VERSION"][2]
    
    return [prefix_join(env["TARGETDIR"]),
            prefix_join(env["TARGETDIR"], "include"),
            prefix_join(env["TARGETDIR"], "lib"),
            prefix_join(env["TARGETDIR"], "bin"),
            prefix_join(env["TARGETDIR"]),
            prefix_join(env["TARGETDIR"], "plugins"),
            str(qt_version_number),
            "8",                       # open source edition
            "Open Source",
            "shared"], definitions

def convert_configuration_back(pyqt_xfeatures):

    reverse_dict = {}
    
    for key, value in conversion_dict.items():
        reverse_dict[value] = key
    
    xfeatures = set()
    
    for xf in pyqt_xfeatures:
        if xf in reverse_dict:
            xfeatures.add(reverse_dict[xf])
    
    return list(xfeatures)

def build_embedded_pyqt(patch = True, configuration_lines = [], extra_qt_defs = []):

    dest_dir = env.get("DESTDIR", "")
    
    # Enter the distribution directory.
    chdir(env["PYQTDIR"])
    
    env_commands = (
        "PATH="+os.path.join(env["TARGET_TOOLS_BIN"])+":"+env["ORIGINAL_PATH"] + " "
        "CROSS_SIPCONFIG="+env["TARGET_PYTHON_SITE_PACKAGES"] + " "
        "QMAKESPEC="+prefix_join(env["TARGETDIR"], "mkspecs", "qws", env["TARGET_PLATFORM_SPEC"]) + " "
        "PYTHONPATH="+env["TARGET_PYTHON_SITE_PACKAGES"] + " "
        "NATIVE_SIP="+os.path.join(env["NATIVE_SIPDIR"], "sipgen", "sip") + " "
        'EXTRA_DEFINES="'+" ".join(extra_qt_defs) + '" '
        )
    
    # Create a configuration file for the configure script to use.
    open("qtdirs.out", "w").write("\n".join(configuration_lines))
    
    if patch:
        # Copy the new configure script and qtdirs.out into the distribution.
        system("cp "+os.path.join(env["PYQTPATCHDIR"], "configure-qws.py")+" .")
        system("python "+os.path.join(env["PYQTPATCHDIR"], "patch_files.py")+" "+os.path.join(env["PYQTPATCHDIR"], "sip")+" sip")
        system("python "+os.path.join(env["PYQTPATCHDIR"], "patch_files.py")+" "+os.path.join(env["PYQTPATCHDIR"], "qpy")+" qpy")
    
    system(
        env_commands + \
        "python" + " configure-qws.py " + \
        "-b "+os.path.join(env["TARGETDIR"], "bin") + " " + \
        "-d "+env["TARGET_PYTHON_SITE_PACKAGES"] + " " + \
        "-l "+env["TARGET_PYTHON_INCLUDE_DIR"] + " " + \
        "-m "+env["TARGET_PYTHON_CONFIG_DIR"] + " " + \
        "-q "+prefix_join(env["TARGETDIR"], "bin", "qmake") + " " + \
        "-v "+os.path.join(env["TARGETDIR"], "share", "sip", "PyQt4") + " " + \
        "-w --confirm-license"
        )
    
    # You may need to fix the -I /usr/include/python2.5 entries in
    # qpy/QtCore/Makefile and qpy/QtGui/Makefile to refer to
    # $TARGETDIR/include/python2.5 instead.
    
    env_commands = "PATH="+os.path.join(env["TARGET_TOOLS_BIN"])+":"+env["ORIGINAL_PATH"]+" "
    system(env_commands + "make -j"+env["TARGET_COMPILERS"])
    
    # Since support for DESTDIR was added later, I've made it optional.
    if env.has_key("DESTDIR"):
        system(env_commands + "make install DESTDIR="+env["DESTDIR"])
    else:
        system(env_commands + "make install")


################################################################################
# Calls to build functions and additional changes to configuration go here.
################################################################################

if __name__ == "__main__":

    py_suffix = os.extsep + "py"
    pyc_suffix = os.extsep + "pyc"
    
    argv = sys.argv[:]
    quiet = read_option(("-q", "--quiet"), argv)
    
    try:
        import local_settings
    except ImportError:
        sys.stderr.write("Usage: %s [-q]\n" % sys.argv[0])
        sys.stderr.write("Copy or link a settings file from Settings to a file "
                         "called local_settings.py in the current directory.\n")
        sys.exit(1)
    
    env = local_settings.env
    
    print "Default settings:"
    print
    for key, value in env.items():
        print key, ":", value
    print
    
    print "Qt configuration options:", local_settings.qt_options
    print "Qt configuration profile:", local_settings.qt_configuration_profile
    print "Extra Qt configuration:", local_settings.extra_qt_configuration
    print "Extra PyQt configuration:", local_settings.extra_pyqt_configuration
    print
    
    if not quiet:
    
        settings_source_file = local_settings.__file__
        if settings_source_file.endswith(pyc_suffix):
            settings_source_file = settings_source_file[:-len(pyc_suffix)] + py_suffix
        
        print "Exit and edit the %s file to change these values." % settings_source_file
        while True:
            a = raw_input("Continue or exit (c|e):")
            if a == "c":
                break
            elif a == "e":
                sys.exit()
    
    # Locate and unpack sources in the current directory, and update the global
    # dictionary of useful variables to expose to child processes.
    locate_sources()
    make_working_directory()
    unpack_sources()
    update_environment()
    
    # Check whether the Qt configuration profile exists before we start.
    qt_configuration_profile = local_settings.qt_configuration_profile

    qconfig_path = os.path.join(env["QTDIR"], "src", "corelib", "global")
    features_path = os.path.join(qconfig_path, "qfeatures" + os.extsep + "txt")
    
    if local_settings.qt_configuration_profile:
    
        additional_qconfig_path = os.path.join(env["QTPATCHDIR"], "src", "corelib", "global")
        configuration_file = "qconfig-" + local_settings.qt_configuration_profile+os.extsep+"h"
        
        config_dirs = [os.pardir, qconfig_path, additional_qconfig_path]
        
        for config_dir in config_dirs:
            config_path = os.path.join(config_dir, configuration_file)
            if os.path.exists(config_path):
                if config_dir != qconfig_path:
                    system("cp " + config_path + " " + os.path.join(qconfig_path, configuration_file))
                break
        else:
            sys.stderr.write("Cannot locate %s.\n" % configuration_file)
            sys.exit(1)
        
        qt_configuration_defs = read_configuration(config_path)
    else:
        qt_configuration_defs = []
    
    extra_qt_configuration = local_settings.extra_qt_configuration
    pyqt_xfeatures = local_settings.extra_pyqt_configuration

    # Add any new PyQt configuration lines to the Qt configuration lines to
    # ensure that included headers are processed properly.
    extra_qt_configuration += convert_configuration_back(pyqt_xfeatures)
    
    # Expand the configuration definitions to include dependencies.
    qt_configuration_defs = expand_configuration(qt_configuration_defs + extra_qt_configuration, features_path)
    
    pyqt_config, pyqt_xfeatures = convert_configuration(qt_configuration_defs)
    
    qt_configuration_defs = list(set(qt_configuration_defs))
    qt_configuration_defs.sort()
    
    print "Final Qt configuration:", qt_configuration_defs
    print "Final PyQt configuration:", pyqt_xfeatures
    print
    
    # If configuration checks were successful, we can create the target
    # directories.
    create_target_directories()
    
    # Copy the target Python configuration into the target include directory.
    copy_target_python_configuration(env)
    
    build_qt(patch = True, qt_options = local_settings.qt_options,
             configuration_profile = qt_configuration_profile,
             extra_configuration = extra_qt_configuration)
    
    build_native_sip()
    build_embedded_sip()
    build_embedded_pyqt(patch = True, configuration_lines = pyqt_config + pyqt_xfeatures,
                        extra_qt_defs = extra_qt_configuration)
    
    sys.exit()
